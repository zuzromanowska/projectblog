from datetime import datetime

from django.db import models

# Create your models here.
MOVIES_LIST = (('The Matrix', 'The Matrix'),
               ('Forrest Gump', 'Forrest Gump'),
               ('The Silence of the Lambs', 'The Silence of the Lambs'),
               ('Avatar', 'Avatar'),
               ('Shrek', 'Shrek'))


class Review(models.Model):
    id = models.AutoField(primary_key=True)
    content = models.TextField(default="")
    date = models.DateField(default=datetime.today().strftime('%Y-%m-%d'))
    movie = models.CharField(max_length=20, default="")
    author = models.CharField(max_length=50, default="")
