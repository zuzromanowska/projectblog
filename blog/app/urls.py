from django.urls import path
from .views import index
from .views import reviews
from .views import add_reviews
from .views import about

urlpatterns = [
    path('', index, name='index'),
    path('reviews/', reviews, name='reviews'),
    path('add_reviews/', add_reviews, name='add_reviews'),
    path('about/', about, name='about'),
]
