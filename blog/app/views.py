from datetime import datetime

from django.shortcuts import render
from . import models, forms


# Create your views here.

def index(request):
    return render(request, 'index.html', {})


def reviews(request):
    reviews_obj = models.Review.objects.all()
    return render(request, 'reviews.html', {'reviews': reviews_obj})


def add_reviews(request):
    if request.method == 'POST':
        form = forms.ReviewsForm(request.POST)
        date = form.data['date']
        datetime.strptime(date, "%Y-%m-%d").date()
        print(date)
        if form.is_valid():
            form.save()
    form = forms.ReviewsForm()
    return render(request, 'add_reviews.html', {'form': form})


def about(request):
    return render(request, 'about.html', {})
