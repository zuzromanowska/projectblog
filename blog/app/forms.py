from datetime import datetime

from django import forms
from . import models


class ReviewsForm(forms.ModelForm):
    class Meta:
        model = models.Review
        fields = ('movie', 'content', 'date', 'author')
        widgets = {
            'movie': forms.Select(choices=models.MOVIES_LIST),
        }
